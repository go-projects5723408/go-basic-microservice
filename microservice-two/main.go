package main

import (
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
)

func getMessageFromService1(c *gin.Context) {
	resp, err := http.Get("http://localhost:8080/hello")
	if err != nil {
		c.JSON(http.StatusServiceUnavailable, gin.H{"error": "Unable to reach the server"})
	} else {
		defer resp.Body.Close()

		body, _ := io.ReadAll(resp.Body)
		c.String(http.StatusOK, string(body))
	}
}

func main() {
	r := gin.Default()
	r.GET("/get-message", getMessageFromService1)
	r.Run(":8081") 
}